﻿namespace Product.DAL
{
    public interface IEntityEntry
    {
        object Entity { get; }
        bool Modified { get; }
        bool Added { get; }
        bool Deleted { get; }
    }

}
