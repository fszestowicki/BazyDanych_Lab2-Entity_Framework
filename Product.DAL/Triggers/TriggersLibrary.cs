﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product.DAL.Triggers
{
    public class TriggersLibrary
    {
        private List<Type> _TypeLibrary = new List<Type>();

        public TriggersLibrary()
        {
            // Tu należy dodać triggery
            _TypeLibrary.Add(typeof(OrderTotalAmountTrigger));
            _TypeLibrary.Add(typeof(OrderHistoryTrigger));
        }

        public IEnumerable<ITriggerBefore<T>> GetTriggersBeforeChanges<T>()
        {
            return GetTriggersOfType<ITriggerBefore<T>>();
        }

        public IEnumerable<ITriggerAfter<T>> GetTriggersAfterChanges<T>()
        {
            return GetTriggersOfType<ITriggerAfter<T>>();
        }

        private IEnumerable<T> GetTriggersOfType<T>()
        {

            IEnumerable<Type> triggerTypesToInstantiate = _TypeLibrary
                .Where(t => typeof(T).IsAssignableFrom(t));
            foreach (var triggerType in triggerTypesToInstantiate)
            {
                T trigger = default(T);

                trigger = (T)Activator.CreateInstance(triggerType);

                if (trigger != null)
                    yield return trigger;
            }
        }

    }

    public interface ITriggerBefore<in T>
    {
        void Execute(T target, UnitOfWork iuow, IEntityEntry entry);
    }

    public interface ITriggerAfter<in T>
    {
        void Execute(T target, UnitOfWork iuow, IEntityEntry entry);
    }
}
