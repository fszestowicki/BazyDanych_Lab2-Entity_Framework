﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Product.DAL.Entities
{
    public class Order
    {
        public Order()
        {
            Products = new HashSet<ProductOrder>();
        }

        public int Id { get; set; }

        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        public virtual ICollection<ProductOrder> Products { get; set; }
        [Column(TypeName = "Money")]
        public decimal TotalValue { get; set; }

        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }

        public string Description { get; set; }
    }
}
