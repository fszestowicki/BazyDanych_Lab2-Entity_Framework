﻿using System.Data.Entity;
using System.Linq;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Product.DAL
{
    public class Repository<T> where T : class
    {
        private readonly DbSet<T> _objectSet;

        public Repository(DbContext objectContext)
        {
            _objectSet = objectContext.Set<T>();
        }

        public IQueryable<T> AsQueryable()
        {
            return _objectSet;
        }

        public IQueryable<T> AsNoTracking()
        {
            return _objectSet.AsNoTracking();
        }

        public T Find(int key)
        {
            return _objectSet.Find(key);
        }

        public void Delete(T entity)
        {
            _objectSet.Remove(entity);
        }

        public void Add(T entity)
        {
            _objectSet.Add(entity);
        }

        public void Attach(T entity)
        {
            _objectSet.Attach(entity);
        }

        public T CreateNew()
        {
            return _objectSet.Create();
        }

        public ObservableCollection<T> ToObservableCollection()
        {
            _objectSet.Load();
            return _objectSet.Local;
        }
    }
}
