﻿using System.Data.Entity.ModelConfiguration;

namespace Product.DAL.Confiugration
{
    public class CustomerConfiguration : EntityTypeConfiguration<Entities.Customer>
    {
        public CustomerConfiguration()
        {
            HasIndex(x => x.NipOrPesel)
                .IsUnique();
        }
    }
}
