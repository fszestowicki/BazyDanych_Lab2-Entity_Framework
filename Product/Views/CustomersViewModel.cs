﻿using System;
using System.Linq;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using System.Windows.Threading;
using Product.DAL.Entities;
using Product.DAL;

namespace Product.Views
{
    public class CustomersViewModel : BindableBase
    {
        public CustomersViewModel()
        {
            SaveCommand = new DelegateCommand(Save);
            ViewLoaded = new DelegateCommand(OnLoad);
            ViewUnloaded = new DelegateCommand(OnUnload);

            Customers = new ObservableCollection<Customer>();

            filteringTimer.Tick += new EventHandler(OnFilter);
            filteringTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
        }

        #region commands
        public ICommand SaveCommand { get; set; }
        private void Save()
        {
            try
            {
                var repo = unitOfWork.GetRepository<Customer>();
                foreach (var c in Customers.Where(x => x.Id == 0))
                    repo.Add(c);

                unitOfWork.SaveChanges();

                MessageBox.Show("Changes saved successfully", "Success", 
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Couldn't save changes. Please make sure, that all fields were entered correctly", 
                    "Saving changes error", 
                    MessageBoxButton.OK, 
                    MessageBoxImage.Error);
            }
        }

        public ICommand ViewLoaded { get; set; }
        private void OnLoad()
        {
            if (unitOfWork == null)
                unitOfWork = new UnitOfWork();

            Filter(null);
        }

        public ICommand ViewUnloaded { get; set; }
        private void OnUnload()
        {
            if (unitOfWork != null)
            {
                unitOfWork.Dispose();
                unitOfWork = null;
            }
        }

        #endregion

        #region properties
        private UnitOfWork unitOfWork;
        DispatcherTimer filteringTimer = new DispatcherTimer();

        private string _filterPattern;
        public string FilterPattern
        {
            get { return _filterPattern; }
            set { SetProperty(ref _filterPattern, value); filteringTimer.Stop(); filteringTimer.Start(); }
        }



        public ObservableCollection<Customer> Customers { get; set; }
        #endregion

        #region methods


        private void OnFilter(object sender, EventArgs e)
        {
            Filter(FilterPattern);
        }

        private void Filter(string pattern)
        {
            filteringTimer.Stop();

            var query = unitOfWork.GetRepository<Customer>().AsQueryable();
            if (!String.IsNullOrWhiteSpace(pattern))
            {
                var words = FilterPattern.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                query = query.Where(x => words
                    .All(w => (x.Name + " " + x.NipOrPesel + " " + x.Address.City + " " 
                        + x.Address.PostalCode + " " + x.Address.Street)
                    .Contains(w)));
            }

            query = query.OrderBy(x => x.Name);

            Customers.Clear();
            foreach (var c in query.ToList())
                Customers.Add(c);
        }
        #endregion
    }
}