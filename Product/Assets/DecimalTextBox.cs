﻿using System;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace Product.Assets
{
    public class DecimalTextBox : TextBox
    {
        protected override void OnPreviewTextInput(TextCompositionEventArgs e)
        {
            try
            {
                var str = new StringBuilder(((TextBox)e.Source).Text).Append(e.Text).Append("0").ToString();

                Decimal.Parse(str);
            }
            catch(Exception)
            {
                e.Handled = false;
            }

            base.OnPreviewTextInput(e);
        }
    }
}
